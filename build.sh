#!/bin/bash

# apply patches
echo "Applying MAI Patches"
patch -uN tensorflow/tensorflow/lite/CMakeLists.txt -i patches/cmake_fix.patch
echo "Done Applying Patches"

mkdir -p build64
cd build64
cmake -DCMAKE_TOOLCHAIN_FILE=/opt/cross_toolchain/aarch64-gnu-7.toolchain.cmake -DTFLITE_ENABLE_GPU=ON -DTFLITE_ENABLE_NNAPI=ON -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" ../tensorflow/tensorflow/lite

echo ""
echo "Building Tensorflow Lite"
cmake --build . -j
echo "Done building Tensorflow Lite"

echo ""
echo "Building Benchmark Model"
cmake --build . -j -t benchmark_model
echo "Done building Benchmark Model"

echo ""
echo "Build Complete!"

cd ..
