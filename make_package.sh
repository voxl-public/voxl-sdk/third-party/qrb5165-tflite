#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
#
# Semi-universal script for making a deb and ipk package. This is shared
# between the vast majority of VOXL-SDK packages
#
# Add the 'timestamp' argument to add a date-timestamp suffix to the deb package
# version. This is used by CI for making nightly and development builds.
#
# author: james@modalai.com
################################################################################

set -e # exit on error to prevent bad ipk from being generated

################################################################################
# Check arguments
################################################################################

USETIMESTAMP=false
MAKE_DEB=true

print_usage(){
	echo ""
	echo " Package the current project into a deb or ipk package."
	echo " You must run build.sh first to build the binaries"
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./make_package.sh deb"
	echo "        Build a DEB package for 865"
	echo ""
	echo "  ./make_package.sh deb timestamp"
	echo "        Build a DEB package with the current timestamp as a"
	echo "        suffix in both the package name and deb filename."
	echo "        This is used by CI for development packages."
	echo ""
	echo ""
}

process_argument () {

	if [ "$#" -ne 1 ]; then
		echo "ERROR process_argument expected 1 argument"
		exit 1
	fi

	## convert argument to lower case for robustness
	arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')
	case ${arg} in
		"")
			#echo "Making Normal Package"
			;;
		"-t"|"timestamp"|"--timestamp")
			echo "using timestamp suffix"
			USETIMESTAMP=true
			;;
		"-d"|"deb"|"debian"|"--deb"|"--debian")
			echo "msking debian package"
			MAKE_DEB=true
			;;
		*)
			echo "invalid option"
			print_usage
			exit 1
	esac
}


## parse all arguments or run wizard
for var in "$@"
do
	process_argument $var
done


################################################################################
# variables
################################################################################
VERSION=$(cat pkg/control/control | grep "Version" | cut -d' ' -f 2)
PACKAGE=$(cat pkg/control/control | grep "Package" | cut -d' ' -f 2)
DEB_NAME=${PACKAGE}_${VERSION}.deb

DATA_DIR=pkg/data
CONTROL_DIR=pkg/control
DEB_DIR=pkg/DEB

echo "Package Name: " $PACKAGE
echo "version Number: " $VERSION

################################################################################
# start with a little cleanup to remove old files
################################################################################
# remove data directory where 'make install' installed to
sudo rm -rf $DATA_DIR
mkdir $DATA_DIR

# remove ipk and deb packaging folders
rm -rf $DEB_DIR

# remove old ipk and deb packages
rm -f *.deb

################################################################################
## install compiled stuff into data directory with 'make install'
## try this for all 3 possible build folders, some packages are multi-arch
## so both 32 and 64 need installing to pkg directory.
################################################################################

DID_BUILD=false

if [[ -d "build64" ]]; then
	DID_BUILD=true
fi

# make sure at least one directory worked
if [ "$DID_BUILD" = false ] &&  [ -f "build.sh" ]; then
	echo "neither build/ build32/ or build64/ were found"
	exit 1
fi

################################################################################
## grab the scattered tensorflow files that we actually need
################################################################################

mkdir -p $DATA_DIR/usr/lib64/
mkdir -p $DATA_DIR/usr/include/
mkdir -p $DATA_DIR/usr/bin/

cd build64/

sudo cp tools/benchmark/benchmark_model ../$DATA_DIR/usr/bin/benchmark_model_mai

# grab everything from the build directory
find . -name '*.a' -exec cp {} ../$DATA_DIR/usr/lib64/ \;
find . -name '*.so' -exec cp {} ../$DATA_DIR/usr/lib64/ \;
find . -name '*.h' -exec cp --parents \{\} ../$DATA_DIR/usr/include/ \;
cd ..

# grab all the headers for tensorflow
cd tensorflow/
find tensorflow/ -name '*.h' -exec cp --parents \{\} ../$DATA_DIR/usr/include/ \;
cd ..

################################################################################
# make a DEB package
################################################################################

if $MAKE_DEB; then
	echo "starting building Debian Package"

	## make a folder dedicated to IPK building and copy the requires debian-binary file in
	mkdir $DEB_DIR

	## copy the control stuff in
	cp -rf $CONTROL_DIR/ $DEB_DIR/DEBIAN
	cp -rf $DATA_DIR/*   $DEB_DIR

	## update version with timestamp if enabled
	if $USETIMESTAMP; then
		dts=$(date +"%Y%m%d%H%M")
		sed -E -i "s/Version.*/&-$dts/" $DEB_DIR/DEBIAN/control
		VERSION="${VERSION}-${dts}"
		echo "new version with timestamp: $VERSION"
	fi

	DEB_NAME=${PACKAGE}_${VERSION}_arm64.deb
	dpkg-deb --build ${DEB_DIR} ${DEB_NAME}

fi

echo "DONE"